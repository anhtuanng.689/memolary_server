import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
  Request,
  UploadedFile,
  BadRequestException,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiParam,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PromiseJSONResponse } from 'src/utils/types';
import { WordDto } from 'src/words/dtos/word.dto';
import { toWordDto } from 'src/words/schemas/word.schema';
import { WordsService } from 'src/words/words.service';
import { CreateLessonDto } from './dtos/create-lesson.dto';
import { InsertLearnedWordsDto } from './dtos/insert-learned-words.dto';
import { LessonCourseDto } from './dtos/lesson-course.dto';
import { LessonDto } from './dtos/lesson.dto';
import { LessonsService } from './lessons.service';

import { imageFileFilter, storage } from 'src/helpers/storage.helper';

@Controller('lessons')
@UseGuards(JwtAuthGuard)
@ApiTags('Lesson')
@ApiBearerAuth()
export class LessonsController {
  constructor(
    private lessonsService: LessonsService,
    private wordsService: WordsService,
  ) {}

  @Post()
  @ApiBody({ type: CreateLessonDto })
  @ApiOperation({ description: 'Create lesson' })
  async createLesson(@Body() createLessonDto: CreateLessonDto) {
    return this.lessonsService.createLesson(createLessonDto);
  }

  @Get(':lesson')
  @ApiParam({
    type: String,
    name: 'lesson',
    required: true,
    description: 'Lesson Id',
  })
  @ApiOperation({ description: 'Get words from lesson' })
  async getLesson(
    @Param('lesson') lesson: string,
    @Request() req,
  ): PromiseJSONResponse<LessonDto> {
    return this.wordsService
      .getWordsInLesson(req.user.id, lesson)
      .then((res) => {
        return {
          data: res,
        };
      });
  }

  @Get(':lesson/learn')
  @ApiParam({
    type: String,
    name: 'lesson',
    required: true,
    description: 'Lesson Id',
  })
  @ApiOperation({ description: 'Learn lesson' })
  async learn(
    @Param('lesson') lesson: string,
    @Request() req,
  ): PromiseJSONResponse<LessonDto> {
    return this.wordsService
      .getWordsToLearn(req.user.id, lesson)
      .then((res) => {
        return {
          data: res,
        };
      });
  }

  @Post(':lesson/learn')
  @ApiParam({
    type: String,
    name: 'lesson',
    required: true,
    description: 'Lesson Id',
  })
  @ApiOperation({ description: 'Submit learned words' })
  async submitLearnedWords(
    @Param('lesson') lesson: string,
    @Body() insertLearnedWords: InsertLearnedWordsDto,
    @Request() req,
  ) {
    return this.wordsService.insertLearnedWords(
      req.user.id,
      insertLearnedWords.words,
    );
  }

  // @Get('abc')
  // @ApiOperation({ description: 'Learn lesson' })
  // async abc() {
  //   const options = {
  //     method: 'GET',
  //     headers: {
  //       'X-RapidAPI-Host': 'wordsapiv1.p.rapidapi.com',
  //       'X-RapidAPI-Key': 'eb9c8ba624msh360092ab651c68fp17a83cjsn5b87178a9c2f',
  //     },
  //   };
  //   console.log('a');

  //   fetch('https://wordsapiv1.p.rapidapi.com/words/love/pronunciation', options)
  //     .then((response) => response.json())
  //     .then((response) => console.log(response))
  //     .catch((err) => console.error(err));

  //   // return await this.lessonsService.insertSimilarWords();
  // }
}
