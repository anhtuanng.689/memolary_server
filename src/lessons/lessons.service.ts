import {
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import { LessonDocument } from './schemas/lesson.schema';
import { CreateLessonDto } from './dtos/create-lesson.dto';
import { WordsService } from 'src/words/words.service';
import { CoursesService } from 'src/courses/courses.service';
import { HttpService } from '@nestjs/axios';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/utils/constants';

@Injectable()
export class LessonsService {
  constructor(
    @InjectModel('Lesson') private lessonModel: Model<LessonDocument>,
    @Inject(forwardRef(() => WordsService))
    private wordsService: WordsService,
    private coursesService: CoursesService,
    private httpService: HttpService,
  ) {}

  async createLesson({ name, course, words }: CreateLessonDto): Promise<void> {
    try {
      const createLesson = { name: name, course: new Types.ObjectId(course) };
      const lesson = await new this.lessonModel(createLesson).save();
      await this.coursesService.insertLessonToCourse(
        lesson._id.toString(),
        course,
      );
      await this.wordsService.insertWordsToLesson(words, lesson._id.toString());
      await this.updateWordsLesson(words.length, lesson._id.toString());
      await this.coursesService.updateWordsCourse(words.length, course);
    } catch (error) {
      console.log(error);

      throw new InternalServerErrorException(error);
    }
  }

  async updateWordsLesson(words: number, lesson: string) {
    await this.lessonModel.findByIdAndUpdate(lesson, {
      $inc: { number: words },
    });
  }

  async findLesson(lesson: string) {
    return await this.lessonModel.findById(lesson);
  }

  async insertSimilarWords(): Promise<Observable<any>> {
    console.log(
      this.httpService.get(
        `https://wordsapiv1.p.rapidapi.com/words/hello/pronunciation`,
      ),
    );
    return this.httpService.get(
      `https://wordsapiv1.p.rapidapi.com/words/hello/pronunciation`,
    );

    // const options = {
    //   method: 'GET',
    //   url: 'https://wordsapiv1.p.rapidapi.com/words/good/typeOf',
    //   headers: {
    //     'X-RapidAPI-Host': 'wordsapiv1.p.rapidapi.com',
    //     'X-RapidAPI-Key': 'eb9c8ba624msh360092ab651c68fp17a83cjsn5b87178a9c2f',
    //   },
    // };

    // axios
    //   .request(options)
    //   .then(function (response) {
    //     console.log(response.data);
    //   })
    //   .catch(function (error) {
    //     console.error(error);
    //   });
  }
}
