import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { LessonCourseDto as LessonCourseDto } from '../dtos/lesson-course.dto';

export type LessonDocument = Lesson & Document;

@Schema({ timestamps: true })
export class Lesson {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: Types.ObjectId, required: true, ref: 'Course' })
  course: Types.ObjectId;

  @Prop({ type: Number, default: 0, required: true })
  number: number;
}

export const LessonSchema = SchemaFactory.createForClass(Lesson);

export function toLessonCourseDto(lesson: Lesson): LessonCourseDto {
  return {
    id: (lesson as any)._id,
    name: lesson.name,
    course: (lesson as any).course,
    number: lesson.number,
  };
}
