import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type LearnDocument = Learn & Document;

@Schema({ timestamps: true })
export class Learn {
    @Prop({ type: Types.ObjectId, required: true, ref: 'User' })
    user: Types.ObjectId;

    @Prop({ type: Types.ObjectId, required: true, ref: 'Word' })
    word: Types.ObjectId;
}

export const LearnSchema = SchemaFactory.createForClass(Learn);
