import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { CreateWordDto } from 'src/words/dtos/create-word.dto';

export class CreateLessonDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  course: string;

  @IsNotEmpty()
  @ApiProperty({ type: [CreateWordDto] })
  words: CreateWordDto[];
}
