import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { WordDto } from 'src/words/dtos/word.dto';

export class InsertLearnedWordsDto {
    @IsOptional()
    @ApiProperty({ type: [String] })
    words: string[];
}
