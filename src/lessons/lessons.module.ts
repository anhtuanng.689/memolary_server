import { HttpModule } from '@nestjs/axios';
import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CoursesModule } from 'src/courses/courses.module';
import { WordsModule } from 'src/words/words.module';
import { LessonsController } from './lessons.controller';
import { LessonsService } from './lessons.service';
import { LessonSchema } from './schemas/lesson.schema';

@Module({
  imports: [
    HttpModule.register({
      headers: {
        'X-RapidAPI-Host': 'wordsapiv1.p.rapidapi.com',
        'X-RapidAPI-Key': 'eb9c8ba624msh360092ab651c68fp17a83cjsn5b87178a9c2f',
      }, // object of headers you want to set
    }),
    MongooseModule.forFeature([{ name: 'Lesson', schema: LessonSchema }]),
    forwardRef(() => WordsModule),
    CoursesModule,
  ],
  providers: [LessonsService],
  controllers: [LessonsController],
  exports: [LessonsService],
})
export class LessonsModule {}
