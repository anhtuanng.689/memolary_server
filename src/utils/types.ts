export type base64str = string;

export type JSONResponse<Data = any> = {
  data?: Data;
};

export type PromiseJSONResponse<Data = any> = Promise<JSONResponse<Data>>;
