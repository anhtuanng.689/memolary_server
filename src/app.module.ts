import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MongooseModule } from '@nestjs/mongoose';
import { CoursesModule } from './courses/courses.module';
import { LessonsModule } from './lessons/lessons.module';
import { WordsModule } from './words/words.module';
import { ReviewsModule } from './reviews/reviews.module';
import { ServeStaticModule } from '@nestjs/serve-static/dist/serve-static.module';
import { join } from 'path';
import { HttpModule } from '@nestjs/axios';
import { ConfigureService } from './configure/configure.service';
import { NotificationsModule } from './notifications/notifications.module';
import { ScheduleModule } from '@nestjs/schedule';
@Module({
  imports: [
    // ServeStaticModule.forRoot({
    //   rootPath: join(__dirname, '..', 'src'),
    // }),
    HttpModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://127.0.0.1:27017/memolary'),
    UsersModule,
    AuthModule,
    CoursesModule,
    LessonsModule,
    WordsModule,
    ReviewsModule,
    NotificationsModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigureService],
})
export class AppModule {
  static port: number;

  constructor(private readonly configureService: ConfigureService) {
    AppModule.port = this.configureService.port as number;
  }
}
