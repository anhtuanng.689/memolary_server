import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/users/dtos/create-user.dto';
import { UserDto } from 'src/users/dtos/user.dto';
import { toUserDto } from 'src/users/schemas/user.schema';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<UserDto> {
    const user = await this.usersService.findOne(email);
    console.log(user);
    if (user && user.password === password) {
      return toUserDto(user);
    }
    return null;
  }

  async login(user: any) {
    console.log(user);

    const payload = { email: user.email, id: user.id };
    console.log('payload: ', payload);

    return {
      accessToken: this.jwtService.sign(payload),
    };
  }

  async register(createUserDto: CreateUserDto) {
    const user = await this.usersService.create(createUserDto);
    console.log('auth:', createUserDto, user);

    return user;
  }
}
