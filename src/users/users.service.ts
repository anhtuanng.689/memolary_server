import { Injectable } from '@nestjs/common';
import { Model, Types } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { toUserDto, User, UserDocument } from './schemas/user.schema';
import { UserDto } from './dtos/user.dto';
import { CreateUserDto } from './dtos/create-user.dto';
import { toCourseDto } from 'src/courses/schemas/course.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private userModel: Model<UserDocument>) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const user = new this.userModel(createUserDto);
    console.log('user:', createUserDto, user);

    return user.save();
  }

  async findAll(): Promise<User[]> {
    return await this.userModel.find();
  }

  async findOne(email: string): Promise<UserDocument> {
    const user = await this.userModel.findOne({ email: email }).exec();
    return user;
  }

  async findUserById(id: string): Promise<UserDto> {
    const user = await this.userModel.findById(id).populate('courses');
    const userTemp = user as any;
    if (userTemp.courses.length > 0) {
      userTemp.courses.map((course) => {
        return toCourseDto(course);
      });
    }
    console.log(user);

    // const courses = await
    return toUserDto(user);
  }

  async insertCourseHistory(user: string, course: string) {
    const userTemp = await this.userModel.findById(user);
    // console.log(userTemp);
    // console.log(userTemp.courses.length);

    let coursesId = [];

    if (userTemp.courses.length >= 0) {
      coursesId = (userTemp.courses as any).filter(
        (courseId) => course != courseId.toString(),
      );
      coursesId.unshift(course);
      // console.log('Ok');
    }
    while (coursesId.length > 10) {
      coursesId.pop();
    }

    // console.log(new Types.ObjectId(coursesId[0]));

    return this.userModel.findByIdAndUpdate(user, {
      courses: coursesId.map((course) => new Types.ObjectId(course)),
    });
  }

  async updateFCMToken(token: string, user: string): Promise<void> {
    return await this.userModel.findByIdAndUpdate(user, { fcmToken: token });
  }

  // 08:00
  async updateRecall(recallWords: number, recallTime: string, user: string): Promise<void> {
    return await this.userModel.findByIdAndUpdate(user, { recallWords: recallWords, recallTime: recallTime });
  }

}
