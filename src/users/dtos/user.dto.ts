import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { Types } from 'mongoose';
import { CourseDto } from 'src/courses/dtos/course.dto';

export class UserDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  id: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  email: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  avatar: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  fcmToken: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  sex: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  phone: string;

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  birthday: Date;

  @IsNotEmpty()
  @ApiProperty({ type: Number })
  recallWords: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  recallTime: string;

  @IsNotEmpty()
  @ApiProperty({ type: [CourseDto] })
  courses: CourseDto[];

  @IsNotEmpty()
  @ApiProperty({ type: Date })
  createdAt: Date;
}
