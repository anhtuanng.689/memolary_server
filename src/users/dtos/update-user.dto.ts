import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEmail, IsNotEmpty } from 'class-validator';

export class UpdateUserDto {
  @IsString()
  @ApiProperty({ type: String })
  fcmToken?: string;

  @IsString()
  @ApiProperty({ type: Number })
  recallWords?: number;

  @IsString()
  @ApiProperty({ type: String })
  recallTime?: string;
}
