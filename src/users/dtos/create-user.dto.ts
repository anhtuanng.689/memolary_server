import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  @ApiProperty({type: String})
  email: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({type: String})
  password: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({type: String})
  name: string;
}
