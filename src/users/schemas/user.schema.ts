import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { CourseDto } from 'src/courses/dtos/course.dto';
import { Course, toCourseDto } from 'src/courses/schemas/course.schema';
import { UserDto } from '../dtos/user.dto';

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  @Prop({ required: true, type: String })
  email: string;

  @Prop({ required: true, type: String })
  password: string;

  @Prop({ required: true, type: String })
  name: string;

  @Prop({
    required: false,
    type: String,
    default:
      'https://img.freepik.com/free-vector/cute-fox-sitting-cartoon-character-animal-nature-isolated_138676-3172.jpg',
  })
  avatar: string;

  @Prop({ required: false, type: String, default: '' })
  fcmToken: string;

  @Prop({ required: false, type: String, default: '' })
  sex: string;

  @Prop({ required: false, type: String, default: '' })
  phone: string;

  @Prop({ required: false, type: Date, default: Date.now() })
  birthday: Date;

  @Prop({ required: false, type: Number, default: 0 })
  recallWords: number;

  @Prop({ required: false, type: String, default: '' })
  recallTime: string;

  @Prop({ required: true, type: [Types.ObjectId], default: [], ref: 'Course' })
  courses: Types.ObjectId[] | Course[];

  @Prop({ required: false, type: Date })
  createdAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);

export function toUserDto(user: User): UserDto {
  return {
    id: (user as any)._id,
    email: user.email,
    name: user.name,
    avatar: user.avatar,
    fcmToken: user.fcmToken,
    sex: user.sex,
    phone: user.phone,
    birthday: user.birthday,
    recallWords: user.recallWords,
    recallTime: user.recallTime,
    courses: user.courses.map((course) => toCourseDto(course)),
    createdAt: user.createdAt,
  };
}
