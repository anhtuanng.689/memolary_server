import {
  Controller,
  Request,
  Post,
  UseGuards,
  Get,
  Put,
  Body,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { AuthService } from 'src/auth/auth.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { LocalAuthGuard } from 'src/auth/local-auth.guard';
import { PromiseJSONResponse } from 'src/utils/types';
import { UpdateUserDto } from './dtos/update-user.dto';
import { UserDto } from './dtos/user.dto';
import { UsersService } from './users.service';

@Controller('user')
@ApiTags('User')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('profile')
  @ApiOperation({ description: 'Get user profile' })
  getProfile(@Request() req): PromiseJSONResponse<UserDto> {
    return this.usersService.findUserById(req.user.id).then((res) => {
      return {
        data: res,
      };
    });
  }

  @Put('fcm')
  @ApiBody({ type: UpdateUserDto })
  @ApiOperation({ description: 'Update FCM token' })
  updateFCMToken(
    @Request() req,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.updateFCMToken(
      updateUserDto.fcmToken,
      req.user.id,
    );
  }

  @Put('recall')
  @ApiBody({ type: UpdateUserDto })
  @ApiOperation({ description: 'Update recall' })
  updateRecall(
    @Request() req,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.updateRecall(
      updateUserDto.recallWords,
      updateUserDto.recallTime,
      req.user.id,
    );
  }
}
