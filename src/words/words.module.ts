import { ReviewsModule } from './../reviews/reviews.module';
import { forwardRef, Module } from '@nestjs/common';
import { WordsService } from './words.service';
import { MongooseModule } from '@nestjs/mongoose';
import { WordSchema } from './schemas/word.schema';
import { LearnSchema } from 'src/lessons/schemas/learn.schema';
import { LessonsModule } from 'src/lessons/lessons.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Word', schema: WordSchema },
      { name: 'Learn', schema: LearnSchema },
    ]),
    forwardRef(() => ReviewsModule),
    forwardRef(() => LessonsModule),
  ],
  providers: [WordsService],
  exports: [WordsService],
})
export class WordsModule {}
