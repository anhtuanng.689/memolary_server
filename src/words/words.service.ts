import {
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { toWordDto, Word, WordDocument } from './schemas/word.schema';
import { Model, Types } from 'mongoose';
import { LearnDocument } from 'src/lessons/schemas/learn.schema';
import { ReviewsService } from 'src/reviews/reviews.service';
import { CreateWordDto } from './dtos/create-word.dto';
import { LessonsService } from 'src/lessons/lessons.service';
import { LessonDto } from 'src/lessons/dtos/lesson.dto';

@Injectable()
export class WordsService {
  constructor(
    @InjectModel('Word') private wordModel: Model<WordDocument>,
    @InjectModel('Learn') private learnModel: Model<LearnDocument>,
    @Inject(forwardRef(() => ReviewsService))
    private reviewsService: ReviewsService,
    @Inject(forwardRef(() => LessonsService))
    private lessonsService: LessonsService,
  ) {}

  async findByIds(ids: string[]) {
    return await this.wordModel.find({
      _id: {
        $in: ids,
      },
    });
  }

  async insertWordsToLesson(words: CreateWordDto[], lesson: string) {
    try {
      const wordsToInsert = words.map((word) => {
        const wordTemp = word;
        (wordTemp as any).lesson = new Types.ObjectId(lesson);
        return wordTemp;
      });
      await this.wordModel.insertMany(wordsToInsert);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getWordsInLesson(user: string, lesson: string): Promise<LessonDto> {
    try {
      const lessonTemp = await this.lessonsService.findLesson(lesson);
      const learnedWords = await this.learnModel.find({
        user: new Types.ObjectId(user),
      });
      const lessonWords = await this.wordModel.find({
        lesson: new Types.ObjectId(lesson),
      });
      const learnedWordsId = learnedWords.map((word) => {
        return word.word.toString();
      });
      const wordsInLesson = lessonWords.map((word) => {
        let isLearned = false;
        if (learnedWordsId.includes(word._id.toString())) {
          isLearned = true;
        }
        (word as any).isLearned = isLearned;
        return toWordDto(word);
        // {
        //   _id: word._id,
        //   word: word.word,
        //   pronounce: word.pronounce,
        //   wordMeaning: word.wordMeaning,
        //   sentence: word.sentence,
        //   sentenceMeaning: word.sentenceMeaning,
        //   isLearned: isLearned,
        // };
      });
      return {
        id: lesson,
        name: lessonTemp.name,
        words: wordsInLesson,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getWordsToLearn(user: string, lesson: string): Promise<LessonDto> {
    try {
      const lessonTemp = await this.lessonsService.findLesson(lesson);
      const learnedWords = await this.learnModel.find({
        user: new Types.ObjectId(user),
      });
      const learnedWordsId = learnedWords.map((word) => {
        return word.word;
      });
      const toLearnTempWords = await this.wordModel
        .find({
          lesson: new Types.ObjectId(lesson),
          _id: { $nin: learnedWordsId },
        })
        .limit(10);
      const toLearnWords = toLearnTempWords.map((word) => {
        let isLearned = false;
        if (learnedWordsId.includes(word._id.toString())) {
          isLearned = true;
        }
        (word as any).isLearned = isLearned;
        return toWordDto(word);
      });
      // const result = toLearnWords.map((word) => {
      //   return {
      //     _id: word._id,
      //     word: word.word,
      //     pronounce: word.pronounce,
      //     wordMeaning: word.wordMeaning,
      //     sentence: word.sentence,
      //     sentenceMeaning: word.sentenceMeaning,
      //   };
      // });
      return {
        id: lesson,
        name: lessonTemp.name,
        words: toLearnWords,
      };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getLearnedWords(user: string): Promise<WordDocument[]> {
    try {
      return await this.learnModel
        .find({
          user: new Types.ObjectId(user),
        })
        .populate('word');
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async insertLearnedWords(user: string, words: string[]) {
    try {
      const wordsToInsert = words.map((word) => {
        const wordTemp = {
          word: new Types.ObjectId(word),
          user: new Types.ObjectId(user),
        };
        return wordTemp;
      });
      this.reviewsService.updateReviewWordsAfterLearn(user, words);

      await this.learnModel.insertMany(wordsToInsert);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
