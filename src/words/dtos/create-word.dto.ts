import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateWordDto {
  @IsNotEmpty()
  @ApiProperty({ type: String })
  word: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  pronounce: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  wordMeaning: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  sentence: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  sentenceMeaning: string;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  image: string;
}
