import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { WordDto } from '../dtos/word.dto';

export type WordDocument = Word & Document;

@Schema({ timestamps: true })
export class Word {
  @Prop({ type: String, required: true })
  word: string;

  @Prop({ type: String, required: true })
  pronounce: string;

  @Prop({ type: String, required: true })
  wordMeaning: string;

  @Prop({ type: String, required: true })
  sentence: string;

  @Prop({ type: String, required: true })
  image: string;

  @Prop({ type: String, required: true })
  sentenceMeaning: string;

  @Prop({ type: Types.ObjectId, required: true, ref: 'Lesson' })
  lesson: Types.ObjectId;

  @Prop({ type: Boolean })
  isLearned: boolean;
}

export const WordSchema = SchemaFactory.createForClass(Word);

export function toWordDto(word: Word): WordDto {
  return {
    id: (word as any)._id,
    word: word.word,
    pronounce: word.pronounce,
    wordMeaning: word.wordMeaning,
    sentence: word.sentence,
    sentenceMeaning: word.sentenceMeaning,
    image: word.image,
    lesson: (word as any).lesson,
    isLearned: word.isLearned ?? true,
  };
}
