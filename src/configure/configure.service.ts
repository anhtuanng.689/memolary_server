import { Injectable } from '@nestjs/common';

@Injectable()
export class ConfigureService {
  public port;

  constructor() {
    this.port = process.env.PORT || 3000;
  }
}
