import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateReviewDto {
  @IsNotEmpty()
  @ApiProperty({
    type: {
      level1: { type: [String] },
      level2: { type: [String] },
      level3: { type: [String] },
      level4: { type: [String] },
      level5: { type: [String] },
    },
  })
  right: {
    level1: string[];
    level2: string[];
    level3: string[];
    level4: string[];
    level5: string[];
  };

  @IsOptional()
  @ApiProperty({
    type: {
      level1: { type: [String] },
      level2: { type: [String] },
      level3: { type: [String] },
      level4: { type: [String] },
      level5: { type: [String] },
    },
  })
  wrong: {
    level1: string[];
    level2: string[];
    level3: string[];
    level4: string[];
    level5: string[];
  };
}
