import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';

export class SkipWord {
  @ApiProperty({ type: String })
  word: string;

  @ApiProperty({ type: Boolean })
  isSkip: boolean;
}
export class UpdateSkipWordsDto {
  @IsNotEmpty()
  @ApiProperty({ type: [SkipWord] })
  words: SkipWord[];
}
