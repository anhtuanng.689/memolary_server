import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { WordDto } from 'src/words/dtos/word.dto';

export class ReviewDto {
  @IsNotEmpty()
  @ApiProperty({
    type: {
      level1: {
        type: [
          {
            word: { type: WordDto },
            lastReview: { type: Date },
            isSkip: { type: Boolean },
          },
        ],
      },
      level2: {
        type: [
          {
            word: { type: WordDto },
            lastReview: { type: Date },
            isSkip: { type: Boolean },
          },
        ],
      },
      level3: {
        type: [
          {
            word: { type: WordDto },
            lastReview: { type: Date },
            isSkip: { type: Boolean },
          },
        ],
      },
      level4: {
        type: [
          {
            word: { type: WordDto },
            lastReview: { type: Date },
            isSkip: { type: Boolean },
          },
        ],
      },
      level5: {
        type: [
          {
            word: { type: WordDto },
            lastReview: { type: Date },
            isSkip: { type: Boolean },
          },
        ],
      },
    },
    required: true,
  })
  level: {
    level1: { word: WordDto; lastReview: Date; isSkip: boolean }[];
    level2: { word: WordDto; lastReview: Date; isSkip: boolean }[];
    level3: { word: WordDto; lastReview: Date; isSkip: boolean }[];
    level4: { word: WordDto; lastReview: Date; isSkip: boolean }[];
    level5: { word: WordDto; lastReview: Date; isSkip: boolean }[];
  };
}
