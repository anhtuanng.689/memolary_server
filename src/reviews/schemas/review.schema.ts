import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { ReviewDto } from '../dto/review.dto';

export type ReviewDocument = Review & Document;

@Schema({ timestamps: true })
export class Review {
  @Prop({ type: Types.ObjectId, required: true, ref: 'User' })
  user: Types.ObjectId;

  @Prop({
    type: {
      level1: {
        type: [
          {
            word: { type: Types.ObjectId },
            lastReview: { type: Date },
            isSkip: { type: Boolean, default: false },
          },
        ],
      },
      level2: {
        type: [
          {
            word: { type: Types.ObjectId },
            lastReview: { type: Date },
            isSkip: { type: Boolean, default: false },
          },
        ],
      },
      level3: {
        type: [
          {
            word: { type: Types.ObjectId },
            lastReview: { type: Date },
            isSkip: { type: Boolean, default: false },
          },
        ],
      },
      level4: {
        type: [
          {
            word: { type: Types.ObjectId },
            lastReview: { type: Date },
            isSkip: { type: Boolean, default: false },
          },
        ],
      },
      level5: {
        type: [
          {
            word: { type: Types.ObjectId },
            lastReview: { type: Date },
            isSkip: { type: Boolean, default: false },
          },
        ],
      },
    },
    required: true,
  })
  level: {
    level1: { word: Types.ObjectId; lastReview: Date; isSkip: boolean }[];
    level2: { word: Types.ObjectId; lastReview: Date; isSkip: boolean }[];
    level3: { word: Types.ObjectId; lastReview: Date; isSkip: boolean }[];
    level4: { word: Types.ObjectId; lastReview: Date; isSkip: boolean }[];
    level5: { word: Types.ObjectId; lastReview: Date; isSkip: boolean }[];
  };
}

export const ReviewSchema = SchemaFactory.createForClass(Review);

export function toReviewDto(review: Review): ReviewDto {
  return {
    level: {
      level1:
        review.level.level1 != null
          ? review.level.level1.map((item) => {
              return {
                word: (item as any).word,
                lastReview: item.lastReview,
                isSkip: item.isSkip,
              };
            })
          : [],
      level2:
        review.level.level2 != null
          ? review.level.level2.map((item) => {
              return {
                word: (item as any).word,
                lastReview: item.lastReview,
                isSkip: item.isSkip,
              };
            })
          : [],
      level3:
        review.level.level3 != null
          ? review.level.level3.map((item) => {
              return {
                word: (item as any).word,
                lastReview: item.lastReview,
                isSkip: item.isSkip,
              };
            })
          : [],
      level4:
        review.level.level4 != null
          ? review.level.level4.map((item) => {
              return {
                word: (item as any).word,
                lastReview: item.lastReview,
                isSkip: item.isSkip,
              };
            })
          : [],
      level5:
        review.level.level5 != null
          ? review.level.level5.map((item) => {
              return {
                word: (item as any).word,
                lastReview: item.lastReview,
                isSkip: item.isSkip,
              };
            })
          : [],
    },
  };
}
