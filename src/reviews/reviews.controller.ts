import {
  Body,
  Controller,
  Get,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PromiseJSONResponse } from 'src/utils/types';
import { ReviewDto } from './dto/review.dto';
import { UpdateReviewDto } from './dto/update-review.dto';
import { UpdateSkipWordsDto } from './dto/update-skip-words.dto';
import { ReviewsService } from './reviews.service';
@Controller('reviews')
@UseGuards(JwtAuthGuard)
@ApiTags('Review')
@ApiBearerAuth()
export class ReviewsController {
  constructor(private reviewsService: ReviewsService) {}

  @Get()
  @ApiOperation({ description: 'Get reviews of user' })
  async getReviewWords(@Request() req): PromiseJSONResponse<ReviewDto> {
    console.log(req.user);

    return this.reviewsService.getAllReviewWords(req.user.id).then((res) => {
      return {
        data: res,
      };
    });
  }

  @Get('/review')
  @ApiOperation({ description: 'Get review' })
  async getReview(@Request() req): PromiseJSONResponse<ReviewDto> {
    return this.reviewsService.getReview(req.user.id).then((res) => {
      return {
        data: res,
      };
    });
  }

  @Put('/review')
  @ApiBody({ type: UpdateReviewDto })
  @ApiOperation({ description: 'Update after review' })
  async updateAfterReview(
    @Body() updateReviewDto: UpdateReviewDto,
    @Request() req,
  ) {
    return this.reviewsService.updateAfterReview(updateReviewDto, req.user.id);
  }

  @Put('/skip')
  @ApiBody({ type: UpdateSkipWordsDto })
  @ApiOperation({ description: 'Update skip words' })
  async updateSkipWord(
    @Body() updateSkipWords: UpdateSkipWordsDto,
    @Request() req,
  ) {
    return this.reviewsService.updateSkipReview(req.user.id, updateSkipWords);
  }
}
