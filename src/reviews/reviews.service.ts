import {
  Body,
  forwardRef,
  Inject,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import { Review, ReviewDocument } from './schemas/review.schema';
import { Type } from 'class-transformer';
import { UpdateReviewDto } from './dto/update-review.dto';
import { ReviewDto } from './dto/review.dto';
import { WordsService } from 'src/words/words.service';
import { toWordDto } from 'src/words/schemas/word.schema';
import { WordDto } from 'src/words/dtos/word.dto';
import { UpdateSkipWordsDto } from './dto/update-skip-words.dto';

@Injectable()
export class ReviewsService {
  constructor(
    @InjectModel('Review') private reviewModel: Model<ReviewDocument>,
    @Inject(forwardRef(() => WordsService)) private wordsService: WordsService,
  ) {}

  async updateAfterReview(
    @Body() updateReviewDto: UpdateReviewDto,
    user: string,
  ) {
    try {
      return await Promise.all([
        this.updateReviewRight(user, updateReviewDto.right.level1, '1'),
        this.updateReviewRight(user, updateReviewDto.right.level2, '2'),
        this.updateReviewRight(user, updateReviewDto.right.level3, '3'),
        this.updateReviewRight(user, updateReviewDto.right.level4, '4'),
        this.updateReviewRight(user, updateReviewDto.right.level5, '5'),
        this.updateReviewWrong(user, updateReviewDto.wrong.level1, '1'),
        this.updateReviewWrong(user, updateReviewDto.wrong.level2, '2'),
        this.updateReviewWrong(user, updateReviewDto.wrong.level3, '3'),
        this.updateReviewWrong(user, updateReviewDto.wrong.level4, '4'),
        this.updateReviewWrong(user, updateReviewDto.wrong.level5, '5'),
      ]);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async updateReviewWrong(user: string, words: string[], level: string) {
    try {
      const reviewWords = await this.reviewModel.findOne({
        user: new Types.ObjectId(user),
      });

      const wordsToPush = words.map((word) => {
        const wordTemp = new Types.ObjectId(word);
        return {
          word: wordTemp,
          lastReview: new Date(),
        };
      });

      if (level == '1') {
        const wordsInLevel = reviewWords.level.level1;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level1': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level1': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '2') {
        const wordsInLevel = reviewWords.level.level2;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level2': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level1': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '3') {
        const wordsInLevel = reviewWords.level.level3;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level3': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level1': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '4') {
        const wordsInLevel = reviewWords.level.level4;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level4': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level1': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '5') {
        const wordsInLevel = reviewWords.level.level5;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level5': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level1': wordsToPush },
          },
          { multi: true },
        );
      }
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  async updateReviewRight(user: string, words: string[], level: string) {
    try {
      const reviewWords = await this.reviewModel.findOne({
        user: new Types.ObjectId(user),
      });

      const wordsToPush = words.map((word) => {
        const wordTemp = new Types.ObjectId(word);
        return {
          word: wordTemp,
          lastReview: new Date(),
        };
      });

      if (level == '1') {
        const wordsInLevel = reviewWords.level.level1;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level1': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level2': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '2') {
        const wordsInLevel = reviewWords.level.level2;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level2': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level3': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '3') {
        const wordsInLevel = reviewWords.level.level3;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level3': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level4': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '4') {
        const wordsInLevel = reviewWords.level.level4;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level4': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level5': wordsToPush },
          },
          { multi: true },
        );
      } else if (level == '5') {
        const wordsInLevel = reviewWords.level.level5;

        const wordsToPop = [];
        let wordTemp;

        words.forEach((word) => {
          wordsInLevel.forEach((temp) => {
            if (temp.word.toString() == word.toString()) {
              wordTemp = temp;
            }
          });

          if (wordTemp != undefined) {
            wordsToPop.push(wordTemp);
          }
        });

        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $pull: {
              'level.level5': {
                $in: wordsToPop,
              },
            },
          },
          { multi: true },
        );
        await this.reviewModel.findOneAndUpdate(
          {
            user: new Types.ObjectId(user),
          },
          {
            $push: { 'level.level5': wordsToPush },
          },
          { multi: true },
        );
      }
    } catch (e) {
      console.log(e);
    }
  }

  async updateSkipOneWord(
    user: string,
    word: string,
    level: string,
    isSkip: boolean,
  ) {
    try {
      console.log(word, level, isSkip);

      let query, update;
      switch (level) {
        case '1':
          query = {
            user: new Types.ObjectId(user),
            'level.level1.word': new Types.ObjectId(word),
          };
          update = { 'level.level1.$.isSkip': isSkip };
          break;
        case '2':
          query = {
            user: new Types.ObjectId(user),
            'level.level2.word': new Types.ObjectId(word),
          };
          update = { 'level.level2.$.isSkip': isSkip };
          break;
        case '3':
          query = {
            user: new Types.ObjectId(user),
            'level.level3.word': new Types.ObjectId(word),
          };
          update = { 'level.level3.$.isSkip': isSkip };
          break;
        case '4':
          query = {
            user: new Types.ObjectId(user),
            'level.level4.word': new Types.ObjectId(word),
          };
          update = { 'level.level4.$.isSkip': isSkip };
          break;
        case '5':
          query = {
            user: new Types.ObjectId(user),
            'level.level5.word': new Types.ObjectId(word),
          };
          update = { 'level.level5.$.isSkip': isSkip };
          break;
      }
      console.log(await this.reviewModel.updateOne(query, update));
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async updateSkipReview(user: string, { words }: UpdateSkipWordsDto) {
    try {
      const promises = [];
      const resultPromises = [];
      for (const word of words) {
        for (let level = 1; level <= 5; level++) {
          promises.push(
            this.updateSkipOneWord(
              user,
              word.word,
              level.toString(),
              word.isSkip,
            ),
          );
        }
        const skipWord = async () => {
          await Promise.all(promises);
        };
        resultPromises.push(skipWord());
      }

      const skip = await Promise.all(resultPromises);

      console.log(skip);
    } catch (e) {
      console.log(e);
    }
  }

  async getReview(user: string): Promise<ReviewDto> {
    try {
      const timeToReviewLevel1 = new Date().setHours(new Date().getHours() - 1);
      const timeToReviewLevel2 = new Date().setHours(new Date().getHours() - 8);
      const timeToReviewLevel3 = new Date().setDate(new Date().getDate() - 1);
      const timeToReviewLevel4 = new Date().setDate(new Date().getDate() - 3);
      const timeToReviewLevel5 = new Date().setDate(new Date().getDate() - 7);

      // const timeToReviewLevel1 = new Date().setHours(new Date().getHours() - 1);
      // const timeToReviewLevel2 = new Date().setHours(new Date().getHours() - 1);
      // const timeToReviewLevel3 = new Date().setHours(new Date().getHours() - 1);
      // const timeToReviewLevel4 = new Date().setHours(new Date().getHours() - 1);
      // const timeToReviewLevel5 = new Date().setHours(new Date().getHours() - 1);

      const review = await this.reviewModel.aggregate([
        {
          $match: { user: new Types.ObjectId(user) },
        },
        {
          $project: {
            level: {
              level1: {
                $filter: {
                  input: '$level.level1',
                  as: 'level1',
                  cond: {
                    $or: [
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level1.lastReview',
                              new Date(timeToReviewLevel1),
                            ],
                          },
                          { $eq: ['$$level1.isSkip', false] },
                        ],
                      },
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level1.lastReview',
                              new Date(timeToReviewLevel1),
                            ],
                          },
                          { $eq: ['$$level1.isSkip', null] },
                        ],
                      },
                    ],
                  },
                },
              },
              level2: {
                $filter: {
                  input: '$level.level2',
                  as: 'level2',
                  cond: {
                    $or: [
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level2.lastReview',
                              new Date(timeToReviewLevel2),
                            ],
                          },
                          { $eq: ['$$level2.isSkip', false] },
                        ],
                      },
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level2.lastReview',
                              new Date(timeToReviewLevel2),
                            ],
                          },
                          { $eq: ['$$level2.isSkip', null] },
                        ],
                      },
                    ],
                  },
                },
              },
              level3: {
                $filter: {
                  input: '$level.level3',
                  as: 'level3',
                  cond: {
                    $or: [
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level3.lastReview',
                              new Date(timeToReviewLevel3),
                            ],
                          },
                          { $eq: ['$$level3.isSkip', false] },
                        ],
                      },
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level3.lastReview',
                              new Date(timeToReviewLevel3),
                            ],
                          },
                          { $eq: ['$$level3.isSkip', null] },
                        ],
                      },
                    ],
                  },
                },
              },
              level4: {
                $filter: {
                  input: '$level.level4',
                  as: 'level4',
                  cond: {
                    $or: [
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level4.lastReview',
                              new Date(timeToReviewLevel4),
                            ],
                          },
                          { $eq: ['$$level4.isSkip', false] },
                        ],
                      },
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level4.lastReview',
                              new Date(timeToReviewLevel4),
                            ],
                          },
                          { $eq: ['$$level4.isSkip', null] },
                        ],
                      },
                    ],
                  },
                },
              },
              level5: {
                $filter: {
                  input: '$level.level5',
                  as: 'level5',
                  cond: {
                    $or: [
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level5.lastReview',
                              new Date(timeToReviewLevel5),
                            ],
                          },
                          { $eq: ['$$level5.isSkip', false] },
                        ],
                      },
                      {
                        $and: [
                          {
                            $lt: [
                              '$$level5.lastReview',
                              new Date(timeToReviewLevel5),
                            ],
                          },
                          { $eq: ['$$level5.isSkip', null] },
                        ],
                      },
                    ],
                  },
                },
              },
            },
          },
        },
      ]);

      const reviewTemp = review[0];

      if (!reviewTemp.level.level1) {
        reviewTemp.level.level1 = [];
      }

      if (!reviewTemp.level.level2) {
        reviewTemp.level.level2 = [];
      }

      if (!reviewTemp.level.level3) {
        reviewTemp.level.level3 = [];
      }

      if (!reviewTemp.level.level4) {
        reviewTemp.level.level4 = [];
      }

      if (!reviewTemp.level.level5) {
        reviewTemp.level.level5 = [];
      }

      const level1Id = [];
      const level2Id = [];
      const level3Id = [];
      const level4Id = [];
      const level5Id = [];

      const level1Temp = reviewTemp.level.level1.map((word) => {
        level1Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level2Temp = reviewTemp.level.level2.map((word) => {
        level2Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level3Temp = reviewTemp.level.level3.map((word) => {
        level3Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level4Temp = reviewTemp.level.level4.map((word) => {
        level4Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level5Temp = reviewTemp.level.level5.map((word) => {
        level5Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });

      // console.log(level1Id);

      const level1WordsTemp = await this.wordsService.findByIds(level1Id);
      const level2WordsTemp = await this.wordsService.findByIds(level2Id);
      const level3WordsTemp = await this.wordsService.findByIds(level3Id);
      const level4WordsTemp = await this.wordsService.findByIds(level4Id);
      const level5WordsTemp = await this.wordsService.findByIds(level5Id);

      const level1Words = level1WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level2Words = level2WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level3Words = level3WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level4Words = level4WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level5Words = level5WordsTemp.map((word) => {
        return toWordDto(word);
      });

      const level1 = [];
      const level2 = [];
      const level3 = [];
      const level4 = [];
      const level5 = [];

      for (let i = 0; i < level1Temp.length; i++) {
        level1.push({
          word: level1Words[i],
          lastReview: level1Temp[i].lastReview,
          isSkip: level1Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level2Temp.length; i++) {
        level2.push({
          word: level2Words[i],
          lastReview: level2Temp[i].lastReview,
          isSkip: level2Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level3Temp.length; i++) {
        level3.push({
          word: level3Words[i],
          lastReview: level3Temp[i].lastReview,
          isSkip: level3Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level4Temp.length; i++) {
        level4.push({
          word: level4Words[i],
          lastReview: level4Temp[i].lastReview,
          isSkip: level4Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level5Temp.length; i++) {
        level5.push({
          word: level5Words[i],
          lastReview: level5Temp[i].lastReview,
          isSkip: level5Temp[i].isSkip ?? false,
        });
      }
      return {
        level: {
          level1: level1,
          level2: level2,
          level3: level3,
          level4: level4,
          level5: level5,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        level: {
          level1: [],
          level2: [],
          level3: [],
          level4: [],
          level5: [],
        },
      };
    }
  }

  async getAllReviewWords(user: string): Promise<ReviewDto> {
    try {
      const reviewTemp = await this.reviewModel.findOne({
        user: new Types.ObjectId(user),
      });

      const level1Id = [];
      const level2Id = [];
      const level3Id = [];
      const level4Id = [];
      const level5Id = [];

      const level1Temp = reviewTemp.level.level1.map((word) => {
        level1Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level2Temp = reviewTemp.level.level2.map((word) => {
        level2Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level3Temp = reviewTemp.level.level3.map((word) => {
        level3Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level4Temp = reviewTemp.level.level4.map((word) => {
        level4Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });
      const level5Temp = reviewTemp.level.level5.map((word) => {
        level5Id.push(word.word.toString());
        return {
          word: word.word.toString(),
          lastReview: word.lastReview,
          isSkip: word.isSkip ?? false,
        };
      });

      console.log(level1Id);

      const level1WordsTemp = await this.wordsService.findByIds(level1Id);
      const level2WordsTemp = await this.wordsService.findByIds(level2Id);
      const level3WordsTemp = await this.wordsService.findByIds(level3Id);
      const level4WordsTemp = await this.wordsService.findByIds(level4Id);
      const level5WordsTemp = await this.wordsService.findByIds(level5Id);

      const level1Words = level1WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level2Words = level2WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level3Words = level3WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level4Words = level4WordsTemp.map((word) => {
        return toWordDto(word);
      });
      const level5Words = level5WordsTemp.map((word) => {
        return toWordDto(word);
      });

      const level1 = [];
      const level2 = [];
      const level3 = [];
      const level4 = [];
      const level5 = [];

      for (let i = 0; i < level1Temp.length; i++) {
        level1.push({
          word: level1Words[i],
          lastReview: level1Temp[i].lastReview,
          isSkip: level1Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level2Temp.length; i++) {
        level2.push({
          word: level2Words[i],
          lastReview: level2Temp[i].lastReview,
          isSkip: level2Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level3Temp.length; i++) {
        level3.push({
          word: level3Words[i],
          lastReview: level3Temp[i].lastReview,
          isSkip: level3Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level4Temp.length; i++) {
        level4.push({
          word: level4Words[i],
          lastReview: level4Temp[i].lastReview,
          isSkip: level4Temp[i].isSkip ?? false,
        });
      }
      for (let i = 0; i < level5Temp.length; i++) {
        level5.push({
          word: level5Words[i],
          lastReview: level5Temp[i].lastReview,
          isSkip: level5Temp[i].isSkip ?? false,
        });
      }
      return {
        level: {
          level1: level1,
          level2: level2,
          level3: level3,
          level4: level4,
          level5: level5,
        },
      };
    } catch (error) {
      console.log(error);
      return {
        level: {
          level1: [],
          level2: [],
          level3: [],
          level4: [],
          level5: [],
        },
      };
    }
  }

  async updateReviewWordsAfterLearn(user: string, words: string[]) {
    try {
      const wordsToUpdate = words.map((word) => {
        const wordTemp = new Types.ObjectId(word);
        return {
          word: wordTemp,
          lastReview: new Date(),
        };
      });
      return await this.reviewModel.findOneAndUpdate(
        {
          user: new Types.ObjectId(user),
        },
        {
          $push: { 'level.level1': wordsToUpdate },
        },
        { upsert: true },
      );
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
