import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PushNotificationDto } from './dtos/push-notification.dto';
import { NotificationsService } from './notifications.service';

@Controller('notifications')
@UseGuards(JwtAuthGuard)
@ApiTags('Notification')
@ApiBearerAuth()
export class NotificationsController {
  constructor(private notificationsService: NotificationsService) {}

  @Post()
  @ApiOperation({ description: 'Push review notifications' })
  async send(@Body() pushNotificationDto: PushNotificationDto) {
    console.log(`method called ${this.send.name}()`);
    return await this.notificationsService.send(pushNotificationDto);
  }
}
