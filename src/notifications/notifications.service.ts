import { Injectable, InternalServerErrorException } from '@nestjs/common';
import {
  Cron,
  CronExpression,
  Interval,
  SchedulerRegistry,
} from '@nestjs/schedule';
import { CronJob } from 'cron';
import * as admin from 'firebase-admin';
import { ServiceAccount } from 'firebase-admin';
import { ReviewsService } from 'src/reviews/reviews.service';
import { toUserDto } from 'src/users/schemas/user.schema';
import { UsersService } from 'src/users/users.service';
import { WordsService } from 'src/words/words.service';
import * as serviceAccount from '../../push-notification-google-service.json';
import { PushNotificationDto } from './dtos/push-notification.dto';

@Injectable()
export class NotificationsService {
  constructor(
    private schedulerRegistry: SchedulerRegistry,
    private usersService: UsersService,
    private reviewsService: ReviewsService,
    private wordsService: WordsService,
  ) {
    admin.initializeApp({
      credential: admin.credential.cert(serviceAccount as ServiceAccount),
    });
  }

  // @Interval(3000)
  @Cron(CronExpression.EVERY_HOUR, { timeZone: 'Asia/Ho_Chi_Minh' })
  async triggerLearnedWordsNotifications() {
    const now = new Date(Date.now());
    const hour = now.getHours();

    const users = await this.usersService.findAll();

    const learnedWordsTemp = await Promise.all(
      users
        .filter((i) => i.fcmToken != '')
        .map(async (user) => {
          const userTemp = toUserDto(user);
          // console.log(userTemp);

          if (
            userTemp.recallWords &&
            userTemp.recallTime &&
            userTemp.recallWords != 0 &&
            userTemp.recallTime != '' &&
            userTemp.fcmToken != '' &&
            this.splitString(userTemp.recallTime) == hour
          ) {
            const user = {
              id: userTemp.id,
              token: userTemp.fcmToken,
              recallWords: userTemp.recallWords,
              recallTime: userTemp.recallTime,
            };
            let words = [];

            const learnedWords = await this.wordsService.getLearnedWords(
              user.id,
            );
            if (learnedWords.length < user.recallWords) {
              console.log('Chua du tu vung de nhac nho');
              return {
                token: '',
                words: [],
              };
            } else {
              while (words.length < user.recallWords) {
                this.shuffleArray(learnedWords);
                const rand = Math.floor(Math.random() * user.recallWords);
                if (!words.includes(learnedWords[rand])) {
                  words.push(learnedWords[rand]);
                }
              }
              // console.log(words);

              return {
                token: user.token,
                words: words,
              };
            }
          } else {
            return {
              token: '',
              words: [],
            };
          }
        }),
    );

    // console.log(learnedWordsTemp);

    // console.log('Triggering Message Sending');
    const notifications = await Promise.all(
      learnedWordsTemp.map(async (recalls) => {
        if (recalls.token == '' || recalls.words == []) {
          return {
            error: 'Error',
          };
        } else {
          recalls.words.map(async (recall) => {
            console.log('Sending...');
            await this.send({
              title: recall.word.word,
              body: recall.word.wordMeaning,
              token: recalls.token,
            });
            return recall;
          });
          return recalls;
        }
      }),
    );
    // console.log('Notification' + notifications);
  }

  // @Cron('* 10 * * * *	', {
  //   name: 'messaging',
  // })
  // @Interval(5000)
  // @Interval(216000000)
  @Cron(CronExpression.EVERY_30_MINUTES, { timeZone: 'Asia/Ho_Chi_Minh' })
  async triggerReviewNotifications() {
    const users = await this.usersService.findAll();

    const reviewTemp = await Promise.all(
      users.map(async (user) => {
        const userTemp = toUserDto(user);
        // console.log(userTemp);

        const review = await this.reviewsService.getReview(
          userTemp.id.toString(),
        );
        // console.log('review: ', review);

        const reviewWords =
          review.level.level1.length +
          review.level.level2.length +
          review.level.level3.length +
          review.level.level4.length +
          review.level.level5.length;

        return {
          token: userTemp.fcmToken,
          number: reviewWords,
        };
      }),
    );
    // console.log(reviewTemp);
    const notifications = await Promise.all(
      reviewTemp
        .filter((i) => i.token != '' && i.number != 0)
        .map(async (review) => {
          if (review.token != '' && review.number > 0) {
            console.log('Sending...');

            await this.send({
              title: 'Đã đến giờ ôn tập từ vựng!!!',
              body: `Bạn có ${review.number} từ vựng cần ôn tập`,
              token: review.token,
            });
          }
          return review;
        }),
    );
    // console.log('Notification' + notifications);
  }

  async send(pushNotificationDto: PushNotificationDto) {
    try {
      const { title, body, token } = pushNotificationDto;

      const payload = {
        notification: {
          title,
          body,
        },
      };

      // console.log(title, body, token);
      // console.log('Pay load ' + payload.notification.title);

      Promise.all([await admin.messaging().sendToDevice(token, payload)]);
      // console.log(`${this.send.name} executed with notification payload`);
    } catch (error) {
      console.log(error);
      throw new InternalServerErrorException(error);
    }
  }

  shuffleArray(arr) {
    for (let i = arr.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [arr[i], arr[j]] = [arr[j], arr[i]];
    }
    return arr;
  }

  splitString(str) {
    return str.substring(0, str.indexOf(':'));
  }
}
