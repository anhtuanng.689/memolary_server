import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsOptional, IsNumber } from 'class-validator';
import { LessonCourseDto } from 'src/lessons/dtos/lesson-course.dto';

export class CourseDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  id: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  image: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ type: String })
  description: string;

  @IsOptional()
  @ApiProperty({ type: [LessonCourseDto] })
  lessons?: LessonCourseDto[];

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  number: number;

  @IsNotEmpty()
  @ApiProperty({ type: Boolean })
  isOwner?: boolean;

  @IsNotEmpty()
  @ApiProperty({ type: String })
  owner?: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ type: Number })
  viewCount: number;
}
