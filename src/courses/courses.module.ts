import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from 'src/users/users.module';

import { CoursesController } from './courses.controller';
import { CoursesService } from './courses.service';
import { CourseSchema } from './schemas/course.schema';
import { UserCourseSchema } from './schemas/user-course.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Course', schema: CourseSchema },
      { name: 'UserCourse', schema: UserCourseSchema },
    ]),
    UsersModule
  ],
  controllers: [CoursesController],
  providers: [CoursesService],
  exports: [CoursesService],
})
export class CoursesModule {}
