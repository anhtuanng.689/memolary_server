import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types, ObjectId } from 'mongoose';
import { CourseDto } from '../dtos/course.dto';
import { Lesson, toLessonCourseDto } from 'src/lessons/schemas/lesson.schema';

export type CourseDocument = Course & Document;

@Schema({ timestamps: true })
export class Course {
  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: true })
  image: string;

  @Prop({ type: String, required: true })
  description: string;

  @Prop({ type: [{ type: Types.ObjectId, ref: 'Lesson' }] })
  lessons: Lesson[] | ObjectId[];

  @Prop({ type: Number, required: true, default: 0 })
  number: number;

  @Prop({ type: Number, required: true, default: 0 })
  viewCount: number;
}

export const CourseSchema = SchemaFactory.createForClass(Course);

export function toCourseDto(course: Course, isOne: boolean = false): CourseDto {
  return isOne
    ? {
        id: (course as any)._id,
        name: course.name,
        image: course.image,
        description: course.description,
        lessons: course.lessons.map((item) => toLessonCourseDto(item)),
        number: course.number,
        viewCount: course.viewCount,
      }
    : {
        id: (course as any)._id,
        name: course.name,
        image: course.image,
        description: course.description,
        number: course.number,
        viewCount: course.viewCount,
      };
}
