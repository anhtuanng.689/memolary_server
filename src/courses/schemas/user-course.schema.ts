import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

export type UserCourseDocument = UserCourse & Document;

@Schema()
export class UserCourse {
  @Prop({ type: Types.ObjectId, required: true, ref: 'User' })
  user: Types.ObjectId;

  @Prop({ type: Types.ObjectId, required: true, ref: 'Course' })
  course: Types.ObjectId;

  @Prop({ Types: Boolean, required: true })
  isOwner: boolean;

  @Prop({ Types: Boolean, required: true, default: false })
  isJoined: boolean;
}

export const UserCourseSchema = SchemaFactory.createForClass(UserCourse);
