import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Query,
  Get,
  Param,
  Post,
  Request,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { PromiseJSONResponse } from 'src/utils/types';
import { CoursesService } from './courses.service';
import { CourseDto } from './dtos/course.dto';
import { CreateCourseDto } from './dtos/create-course.dto';
import { Course, toCourseDto } from './schemas/course.schema';

@Controller('courses')
@UseGuards(JwtAuthGuard)
@ApiTags('Course')
@ApiBearerAuth()
export class CoursesController {
  constructor(private coursesService: CoursesService) {}

  @Post()
  @ApiBody({ type: CreateCourseDto })
  @ApiOperation({ description: 'Create course' })
  createCourse(@Body() body: CreateCourseDto, @Request() req) {
    return this.coursesService.createCourse(body, req.user.id);
  }

  @Get('/all')
  @ApiOperation({ description: 'Get all courses' })
  getAllCourses(): PromiseJSONResponse<CourseDto[]> {
    return this.coursesService.getAllCourses().then((res) => ({
      data: res.map((item) => toCourseDto(item)),
    }));
  }

  @Get('/popular')
  @ApiOperation({ description: 'Get popular courses' })
  getPopularCourses(): PromiseJSONResponse<CourseDto[]> {
    return this.coursesService.getPopularCourses().then((res) => ({
      data: res.map((item) => toCourseDto(item)),
    }));
  }

  @Get()
  @ApiOperation({ description: 'Get my courses' })
  getMyCourses(@Request() req): PromiseJSONResponse<CourseDto[]> {
    return this.coursesService.getMyCourses(req.user.id).then((res) => ({
      data: res.map((item) => toCourseDto(item)),
    }));
  }

  @Get('/search')
  @ApiQuery({
    type: String,
    name: 'name',
    required: true,
    description: 'Course name',
  })
  @ApiOperation({ description: 'Search course by name' })
  findByName(@Query('name') name: string): PromiseJSONResponse<CourseDto[]> {
    return this.coursesService.findByName(name).then((res) => ({
      data: res.map((item) => toCourseDto(item)),
    }));
  }

  @Get(':course')
  @ApiParam({
    type: String,
    name: 'course',
    required: true,
    description: 'Course Id',
  })
  @ApiOperation({ description: 'Get course by id' })
  findOne(
    @Request() req,
    @Param('course') id: string,
  ): PromiseJSONResponse<any> {
    return this.coursesService.findOne(req.user.id, id).then((res) => {
      return {
        data: res,
        // data: toCourseDto(res, true),
      };
    });
  }

  @Post(':course/join')
  @ApiParam({
    type: String,
    name: 'course',
    required: true,
    description: 'Course Id',
  })
  @ApiOperation({ description: 'Join course by id' })
  joinCourse(@Request() req, @Param('course') id: string) {
    return this.coursesService.joinCourse(req.user.id, id);
  }

  @Delete(':course')
  @ApiParam({
    type: String,
    name: 'course',
    required: true,
    description: 'Course Id',
  })
  @ApiOperation({ description: 'Delete course' })
  deleteCourse(@Param('course') id: string) {
    return this.coursesService.deleteOne(id);
  }
}
