import { Model } from 'mongoose';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Course, CourseDocument, toCourseDto } from './schemas/course.schema';
import { CreateCourseDto } from './dtos/create-course.dto';
import { Types } from 'mongoose';
import { UserCourseDocument } from './schemas/user-course.schema';
import { UsersService } from 'src/users/users.service';
import { CourseDto } from './dtos/course.dto';
import { toLessonCourseDto } from 'src/lessons/schemas/lesson.schema';
import { toUserDto } from 'src/users/schemas/user.schema';

@Injectable()
export class CoursesService {
  constructor(
    @InjectModel('Course') private courseModel: Model<CourseDocument>,
    @InjectModel('UserCourse')
    private userCourseModel: Model<UserCourseDocument>,
    private usersService: UsersService,
  ) {}

  async createCourse(
    { name, image, description }: CreateCourseDto,
    userId: string,
  ): Promise<void> {
    const course = {
      name: name,
      image: image,
      description: description,
    };
    const courseResult = await new this.courseModel(course).save();
    const userCourse = {
      user: new Types.ObjectId(userId),
      course: (courseResult as any)._id,
      isOwner: true,
      isJoined: true,
    };
    await new this.userCourseModel(userCourse).save();
  }

  async joinCourse(user: string, courseId: string): Promise<void> {
    const userCourse = {
      user: new Types.ObjectId(user),
      course: new Types.ObjectId(courseId),
      isOwner: false,
      isJoined: true,
    };
    await new this.userCourseModel(userCourse).save();
  }

  async getPopularCourses(): Promise<Course[]> {
    return await this.courseModel.find().sort({ viewCount: -1 }).limit(5);
  }

  async getAllCourses(): Promise<Course[]> {
    return await this.courseModel.find();
  }

  async getMyCourses(userId: string): Promise<Course[]> {
    const userCourse = await this.userCourseModel.find({
      user: new Types.ObjectId(userId),
    });
    console.log(userCourse);
    if (userCourse != []) {
      const coursesId = userCourse.map((item) => item.course.toString());
      return await this.courseModel.find({
        _id: {
          $in: coursesId,
        },
      });
    }
    return [];
  }

  async findOne(userId: string, courseId: string): Promise<CourseDto> {
    await this.usersService.insertCourseHistory(userId, courseId);
    await this.courseModel.findByIdAndUpdate(courseId, {
      $inc: {
        viewCount: 1,
      },
    });
    const userCourse = await this.userCourseModel
      .findOne({
        user: new Types.ObjectId(userId),
        course: new Types.ObjectId(courseId),
      })
      .populate('user');
    console.log(userCourse);

    if (userCourse == null) {
      const owner = await this.userCourseModel
        .findOne({
          course: new Types.ObjectId(courseId),
          isOwner: true,
        })
        .populate('user');
      const user = (owner as any).user;
      const courseTemp = await this.courseModel
        .findById(courseId)
        .populate('lessons');
      const course = {
        id: courseTemp.id,
        name: courseTemp.name,
        image: courseTemp.image,
        description: courseTemp.description,
        lessons: courseTemp.lessons.map((item) => toLessonCourseDto(item)),
        number: courseTemp.number,
        isOwner: false,
        isJoined: false,
        owner: user.name,
        viewCount: courseTemp.viewCount,
      };
      return course;
    } else {
      const user = (userCourse as any).user;

      const courseTemp = await this.courseModel
        .findById(courseId)
        .populate('lessons');
      const course = {
        id: courseTemp.id,
        name: courseTemp.name,
        image: courseTemp.image,
        description: courseTemp.description,
        lessons: courseTemp.lessons.map((item) => toLessonCourseDto(item)),
        number: courseTemp.number,
        isOwner: userCourse.isOwner,
        isJoined: true,
        owner: user.name,
        viewCount: courseTemp.viewCount,
      };

      return course;
    }
  }

  async findByName(name: string): Promise<Course[]> {
    return this.courseModel
      .find({ name: new RegExp(name, 'i') })
      .populate('lessons');
  }

  async deleteOne(courseId: string): Promise<Course | null> {
    await this.userCourseModel.findOneAndDelete({
      course: new Types.ObjectId(courseId),
    });
    return await this.courseModel.findByIdAndDelete(courseId);
  }

  async insertLessonToCourse(lesson: string, course: string) {
    try {
      await this.courseModel.findByIdAndUpdate(course, {
        $push: { lessons: lesson },
      });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async updateWordsCourse(words: number, course: string) {
    await this.courseModel.findByIdAndUpdate(course, {
      $inc: { number: words },
    });
  }
}
